package com.ciandt.ldc.bll;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.enums.LogisticCenterZoneEnum;
import com.ciandt.ldc.model.PackageModel;
import com.ciandt.ldc.model.StepModel;
import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class TransferPackageProcedureTest {

	private List<PackageModel> packages;

	@Before
	public void setUp() {
		packages = Arrays.asList(new PackageModel("validPackageId1", new BigDecimal(1.3)),
				new PackageModel("validPackageId2", new BigDecimal(2.3)),
				new PackageModel("validPackageId3", new BigDecimal(4.3)),
				new PackageModel("validPackageId4", new BigDecimal(4.3)),
				new PackageModel("validPackageId5", new BigDecimal(5.3)));
	}

	@Test
	public void createSteps_withEmpyList_shouldReturnEmptyList() {

		List<StepModel> steps = TransferPackageProcedure.createSteps(new ArrayList<>());

		assertThat(steps.size(), is(0));

	}

	@Test
	public void createSteps_withValidData_shouldReturnSteps() {

		List<StepModel> steps = TransferPackageProcedure.createSteps(packages);

		assertThat((double) steps.size(), is(Math.pow(2, packages.size()) - 1));
		assertThat(steps, is(notNullValue()));

	}

	@Test
	public void getSteps_withValidOnePackage_shouldReturnSameSteps() {

		List<StepModel> steps = TransferPackageProcedure
				.createSteps(Arrays.asList(new PackageModel("validPackageId1", new BigDecimal(1.3))));

		assertThat(new Gson().toJson(steps).equals(new Gson().toJson(Arrays.asList(new StepModel(1, "validPackageId1",
				LogisticCenterZoneEnum.SUPPLY_ZONE, LogisticCenterZoneEnum.TRUCK_ZONE)))), is(true));
	}

	@Test
	public void getSteps_withValidTwoPackages_shouldReturnSameSteps() {

		List<StepModel> steps = TransferPackageProcedure
				.createSteps(Arrays.asList(new PackageModel("validPackageId1", new BigDecimal(1.3)),
						new PackageModel("validPackageId2", new BigDecimal(2.3))));

		assertThat(new Gson().toJson(steps)
				.equals(new Gson().toJson(Arrays.asList(
						new StepModel(1, "validPackageId1", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRANSFER_ZONE),
						new StepModel(2, "validPackageId2", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE),
						new StepModel(3, "validPackageId1", LogisticCenterZoneEnum.TRANSFER_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE)))),
				is(true));
	}

	@Test
	public void getSteps_withValidThreePackages_shouldReturnSameSteps() {

		List<StepModel> steps = TransferPackageProcedure
				.createSteps(Arrays.asList(new PackageModel("validPackageId1", new BigDecimal(1.3)),
						new PackageModel("validPackageId2", new BigDecimal(2.3)),
						new PackageModel("validPackageId3", new BigDecimal(4.3))));

		assertThat(new Gson().toJson(steps)
				.equals(new Gson().toJson(Arrays.asList(
						new StepModel(1, "validPackageId1", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE),
						new StepModel(2, "validPackageId2", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRANSFER_ZONE),
						new StepModel(3, "validPackageId1", LogisticCenterZoneEnum.TRUCK_ZONE,
								LogisticCenterZoneEnum.TRANSFER_ZONE),
						new StepModel(4, "validPackageId3", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE),
						new StepModel(5, "validPackageId1", LogisticCenterZoneEnum.TRANSFER_ZONE,
								LogisticCenterZoneEnum.SUPPLY_ZONE),
						new StepModel(6, "validPackageId2", LogisticCenterZoneEnum.TRANSFER_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE),
						new StepModel(7, "validPackageId1", LogisticCenterZoneEnum.SUPPLY_ZONE,
								LogisticCenterZoneEnum.TRUCK_ZONE)))),
				is(true));

	}

}
