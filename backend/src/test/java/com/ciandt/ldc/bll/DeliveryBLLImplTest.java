package com.ciandt.ldc.bll;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.dao.DeliveryDAO;
import com.ciandt.ldc.dao.PackageDAO;
import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.entity.PackageEntity;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.PackageModel;
import com.ciandt.ldc.model.StepModel;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryBLLImplTest {

	@InjectMocks
	private DeliveryBLLImpl bll;

	@Mock
	private DeliveryDAO deliveryDAO;

	@Mock
	private PackageDAO packageDAO;

	private DeliveryModel validDeliveryModel;

	private DeliveryEntity validDeliveryEntity;

	@Before
	public void setUp() {
		validDeliveryModel = new DeliveryModel();
		validDeliveryModel.setDeliveryId("validDeliveryId");
		validDeliveryModel.setVehicle("validVehicleId");
		validDeliveryModel.setPackages(Arrays.asList(new PackageModel("validPackageId", new BigDecimal(1.3))));

		validDeliveryEntity = new DeliveryEntity();
		validDeliveryEntity.setDeliveryId("validDeliveryId");
		validDeliveryEntity.setVehicleId("validVehicleId");
		validDeliveryEntity.setPackages(
				Arrays.asList(new PackageEntity(new PackageModel("validPackageId", new BigDecimal(1.3)), null)));
	}

	@Test
	public void receiveDelivery_withValidData_shouldSucceed() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);

		boolean result = bll.receiveDelivery(validDeliveryModel);

		assertThat(result, is(true));

	}

	@Test
	public void createSteps_withValidId_shouldSucceed() {

		when(deliveryDAO.get(any(String.class))).thenReturn(validDeliveryEntity);

		List<StepModel> steps = bll.createSteps("validId");

		assertThat(steps, is(notNullValue()));

	}

	@Test(expected = DeliveryStateException.class)
	public void createSteps_withInvalidId_shouldThrowError() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);

		bll.createSteps("invalidId");

	}

}
