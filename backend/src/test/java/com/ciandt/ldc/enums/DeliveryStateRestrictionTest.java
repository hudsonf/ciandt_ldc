package com.ciandt.ldc.enums;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryStateRestrictionTest {

	@Test
	public void execute_shouldSucceed() {

		assertTrue(DeliveryStateRestriction.values().length == 3);
		assertThat(DeliveryStateRestriction.valueOf("INCONSISTENT_DATA"),
				is(DeliveryStateRestriction.INCONSISTENT_DATA));
		assertThat(DeliveryStateRestriction.valueOf("NOT_FOUND"), is(DeliveryStateRestriction.NOT_FOUND));
		assertThat(DeliveryStateRestriction.valueOf("UNKNOW_REASON"), is(DeliveryStateRestriction.UNKNOW_REASON));

	}
}
