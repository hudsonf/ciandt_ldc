package com.ciandt.ldc.enums;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.enums.LogisticCenterZoneEnum;

@RunWith(MockitoJUnitRunner.class)
public class LogisticCenterZoneEnumTest {

	@Test
	public void execute_shouldSucceed() {

		assertTrue(LogisticCenterZoneEnum.values().length == 3);
		assertThat(LogisticCenterZoneEnum.valueOf("TRUCK_ZONE"), is(LogisticCenterZoneEnum.TRUCK_ZONE));
		assertThat(LogisticCenterZoneEnum.valueOf("TRANSFER_ZONE"), is(LogisticCenterZoneEnum.TRANSFER_ZONE));
		assertThat(LogisticCenterZoneEnum.valueOf("SUPPLY_ZONE"), is(LogisticCenterZoneEnum.SUPPLY_ZONE));

		assertThat(LogisticCenterZoneEnum.TRUCK_ZONE.getName(), is("zona do caminhão"));
		assertThat(LogisticCenterZoneEnum.TRANSFER_ZONE.getName(), is("zona de transferência"));
		assertThat(LogisticCenterZoneEnum.SUPPLY_ZONE.getName(), is("zona de abastecimento"));
	}

}
