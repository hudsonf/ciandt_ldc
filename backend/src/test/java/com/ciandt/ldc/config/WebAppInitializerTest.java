package com.ciandt.ldc.config;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import javax.servlet.ServletRegistration.Dynamic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class WebAppInitializerTest {

	@InjectMocks
	private WebAppInitializer webAppInitializer;

	@Mock
	private Dynamic dynamic;

	@Test
	public void customizeRegistration_withInterface_shouldSucceed() {

		webAppInitializer.customizeRegistration(dynamic);

	}

	@Test
	public void getMethods_withInterface_shouldSucceed() {

		assertThat(webAppInitializer.getServletMappings(), equalTo(new String[] { "/" }));
		assertThat(webAppInitializer.getServletConfigClasses(), equalTo(null));
		assertThat(webAppInitializer.getRootConfigClasses(), equalTo(new Class[] { AppConfig.class }));

	}

}
