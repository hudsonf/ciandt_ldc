package com.ciandt.ldc.config.general;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DBDataConfigurationTest {

	@Test
	public void getDBUrl_withValidData_shouldSucceed() {

		assertThat(DBDataConfiguration.getDBUrl(), is(notNullValue()));
	}

	@Test
	public void getDBUsername_withValidData_shouldSucceed() {

		assertThat(DBDataConfiguration.getDBUsername(), is(notNullValue()));
	}

	@Test
	public void getDBPassword_withValidData_shouldSucceed() {

		assertThat(DBDataConfiguration.getDBPassword(), is(notNullValue()));
	}

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor<DBDataConfiguration> constructor = DBDataConfiguration.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		constructor.newInstance();
	}

}
