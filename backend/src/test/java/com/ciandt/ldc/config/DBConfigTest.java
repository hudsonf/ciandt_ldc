package com.ciandt.ldc.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DBConfigTest {

	@InjectMocks
	private DBConfig dbConfig;

	@Test
	public void sessionFactory_withValidData_shouldSucceed() {

		dbConfig.sessionFactory();

	}

	@Test
	public void getDataSource_withValidData_shouldSucceed() {

		dbConfig.getDataSource();

	}

	@Test
	public void hibernateTransactionManager_withValidData_shouldSucceed() {

		dbConfig.hibernateTransactionManager();

	}

}
