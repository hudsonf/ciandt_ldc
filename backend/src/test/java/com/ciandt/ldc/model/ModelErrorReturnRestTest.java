package com.ciandt.ldc.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ModelErrorReturnRestTest {

	@Test
	public void get_getDeliveryModelWithPackageList_ShouldReturnDeliveryModelWithPackageList() {

		ModelErrorReturnRest modelErrorReturnRest = new ModelErrorReturnRest(Arrays.asList("error"));

		assertThat(modelErrorReturnRest, notNullValue());
		assertThat(modelErrorReturnRest.getErrors(), notNullValue());
		assertThat(modelErrorReturnRest.getErrors().get(0), equalTo("error"));

	}
}
