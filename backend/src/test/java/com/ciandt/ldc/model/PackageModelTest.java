package com.ciandt.ldc.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PackageModelTest {

	@Test
	public void constructor_WithData_shoulCreate() {

		PackageModel packageModel = new PackageModel("packageId", BigDecimal.TEN);

		assertThat(packageModel, notNullValue());
		assertThat(packageModel.getId(), equalTo("packageId"));
		assertThat(packageModel.getWeight(), equalTo(BigDecimal.TEN));

	}

	@Test
	public void constructorAndGetterAndSetters_WithData_shoulCreate() {

		PackageModel packageModel = new PackageModel();

		packageModel.setId("packageId");
		packageModel.setWeight(BigDecimal.TEN);
		assertThat(packageModel, notNullValue());
		assertThat(packageModel.getId(), equalTo("packageId"));
		assertThat(packageModel.getWeight(), equalTo(BigDecimal.TEN));

	}

}
