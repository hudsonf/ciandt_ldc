package com.ciandt.ldc.model;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReturnModelTest {

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor<ReturnModel> constructor = ReturnModel.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		constructor.newInstance();
	}

	@Test
	public void get_getDeliveryModelWithPackageList_ShouldReturnDeliveryModelWithPackageList() {

		DeliveryModel deliveryModel = ReturnModel.getDeliveryModelWithPackageList();

		assertThat(deliveryModel, notNullValue());
		assertThat(deliveryModel.getPackages(), notNullValue());
		assertThat(deliveryModel.getPackages().get(0), notNullValue());

	}

}
