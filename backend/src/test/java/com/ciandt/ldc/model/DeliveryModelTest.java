package com.ciandt.ldc.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryModelTest {

	@Test
	public void constructorAndGetterAndSetters_WithData_shoulCreate() {

		DeliveryModel deliveryModel = new DeliveryModel();
		PackageModel packageModel = new PackageModel();

		deliveryModel.setVehicle("vehicleId");
		deliveryModel.setDeliveryId("deliveryId");
		deliveryModel.setPackages(Arrays.asList(packageModel));
		assertThat(deliveryModel, notNullValue());
		assertThat(deliveryModel.getVehicle(), equalTo("vehicleId"));
		assertThat(deliveryModel.getDeliveryId(), equalTo("deliveryId"));
		assertThat(deliveryModel.getPackages(), equalTo(Arrays.asList(packageModel)));

	}

}
