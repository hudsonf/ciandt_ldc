package com.ciandt.ldc.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.entity.PackageEntity;

@RunWith(MockitoJUnitRunner.class)
public class ModelFactoryTest {

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor<ModelFactory> constructor = ModelFactory.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		constructor.newInstance();
	}

	@Test
	public void convert_withNullDeliveryEntityAndNullModelReturn_ShouldReturnNull() {

		assertThat(ModelFactory.convert((DeliveryEntity) null, null), nullValue());

	}

	@Test
	public void convert_withNullDeliveryEntityAndValidReturnModel_ShouldReturnNull() {

		assertThat(ModelFactory.convert(null, ReturnModel.getDeliveryModelWithPackageList()), nullValue());

	}

	@Test
	public void convert_withEmptyDeliveryEntityAndNullReturnModel_ShouldReturnNull() {

		assertThat(ModelFactory.convert(new DeliveryEntity(), null), nullValue());

	}

	@Test
	public void convert_withEmptyDeliveryEntityAndValidReturnModel_ShouldReturnData() {

		assertThat(ModelFactory.convert(new DeliveryEntity(), ReturnModel.getDeliveryModelWithPackageList()),
				notNullValue());

	}

	@Test
	public void convert_withCompleteDeliveryEntityAndValidReturnModel_ShouldConvertCompletly() {

		DeliveryEntity deliveryEntity = createDeliveryEntity();
		DeliveryModel deliveryModel = ModelFactory.convert(deliveryEntity,
				ReturnModel.getDeliveryModelWithPackageList());
		assertThat(deliveryModel, notNullValue());
		assertThat(deliveryEntity.getDeliveryId(), equalTo(deliveryModel.getDeliveryId()));
		assertThat(deliveryEntity.getVehicleId(), equalTo(deliveryModel.getVehicle()));
		assertThat(deliveryEntity.getPackages().size(), equalTo(deliveryModel.getPackages().size()));
		assertThat(deliveryEntity.getPackages().get(0).getPackageId(),
				equalTo(deliveryModel.getPackages().get(0).getId()));
		assertThat(deliveryEntity.getPackages().get(0).getWeight(),
				equalTo(deliveryModel.getPackages().get(0).getWeight()));

	}

	@Test
	public void convert_withDeliveryEntityWithoutPackageListAndValidReturnModel_ShouldReturnEmptyList() {

		DeliveryEntity deliveryEntity = createDeliveryEntity();
		deliveryEntity.setPackages(null);
		DeliveryModel deliveryModel = ModelFactory.convert(deliveryEntity,
				ReturnModel.getDeliveryModelWithPackageList());
		assertThat(deliveryModel, notNullValue());
		assertThat(deliveryModel.getPackages(), notNullValue());

	}

	@Test
	public void convert_withDeliveryEntityAndValidReturnModelWithoutPackageList_ShouldReturnEmptyListForPackages() {

		DeliveryEntity deliveryEntity = createDeliveryEntity();
		DeliveryModel returnModel = ReturnModel.getDeliveryModelWithPackageList();
		returnModel.setPackages(null);
		DeliveryModel deliveryModel = ModelFactory.convert(deliveryEntity, returnModel);
		assertThat(deliveryModel, notNullValue());
		assertThat(deliveryModel.getPackages(), notNullValue());

	}

	@Test
	public void convert_withNullPackageEntityAndReturnModelNull_ShouldReturnNull() {

		assertThat(ModelFactory.convert((PackageEntity) null, null), nullValue());

	}

	@Test
	public void convert_withPackageEntityNullAndValidReturnModel_ShouldReturnNull() {

		assertThat(ModelFactory.convert((PackageEntity) null, new PackageModel()), nullValue());

	}

	@Test
	public void convert_withPackageEntityAndNullReturnModel_ShouldReturnNull() {

		assertThat(ModelFactory.convert(createPackageEntity(), null), nullValue());

	}

	@Test
	public void convert_withValidPackageEntityAndValidReturnModel_ShouldReturnNull() {

		PackageEntity packageEntity = createPackageEntity();
		PackageModel packageModel = ModelFactory.convert(packageEntity, new PackageModel());
		assertThat(packageModel, notNullValue());
		assertThat(packageModel.getId(), equalTo(packageEntity.getPackageId()));
		assertThat(packageModel.getWeight(), equalTo(packageEntity.getWeight()));

	}

	private PackageEntity createPackageEntity() {
		PackageEntity packageEntity = new PackageEntity();
		packageEntity.setPackageId("packageId");
		packageEntity.setWeight(BigDecimal.TEN);

		return packageEntity;
	}

	private DeliveryEntity createDeliveryEntity() {
		DeliveryEntity deliveryEntity = new DeliveryEntity();
		deliveryEntity.setDeliveryId("deliveryId");
		deliveryEntity.setVehicleId("vehicleId");

		PackageEntity packageEntity = new PackageEntity();
		packageEntity.setPackageId("packageId");
		packageEntity.setWeight(BigDecimal.TEN);
		deliveryEntity.setPackages(Arrays.asList(packageEntity));

		return deliveryEntity;
	}

}
