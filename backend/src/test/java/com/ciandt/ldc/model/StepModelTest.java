package com.ciandt.ldc.model;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.enums.LogisticCenterZoneEnum;
import com.ciandt.ldc.model.StepModel;

@RunWith(MockitoJUnitRunner.class)
public class StepModelTest {

	@Test
	public void constructorAndGetterAndSetters_WithAllData_shoulCreate() {

		StepModel stepModel = new StepModel(1L, "packageId", LogisticCenterZoneEnum.SUPPLY_ZONE,
				LogisticCenterZoneEnum.TRANSFER_ZONE);

		assertThat(stepModel, notNullValue());
		assertThat(stepModel.getStep(), equalTo(1L));
		assertThat(stepModel.getPackageId(), equalTo("packageId"));
		assertThat(stepModel.getFrom(), equalTo(LogisticCenterZoneEnum.SUPPLY_ZONE.getName()));
		assertThat(stepModel.getTo(), equalTo(LogisticCenterZoneEnum.TRANSFER_ZONE.getName()));

	}

	@Test
	public void constructorAndGetterAndSetters_WithoutFromAndToData_shoulCreate() {

		StepModel stepModel = new StepModel(1L, "packageId", null, null);

		assertThat(stepModel, notNullValue());
		assertThat(stepModel.getStep(), equalTo(1L));
		assertThat(stepModel.getPackageId(), equalTo("packageId"));
		assertThat(stepModel.getFrom(), equalTo(null));
		assertThat(stepModel.getTo(), equalTo(null));

	}
}
