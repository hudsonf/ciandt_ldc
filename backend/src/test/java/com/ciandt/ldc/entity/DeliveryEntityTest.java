package com.ciandt.ldc.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.model.DeliveryModel;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryEntityTest {

	@Test
	public void publicConstructor_withoutParameters_shouldSucceed() {

		assertThat(new DeliveryEntity(), is(notNullValue()));

	}

	@Test
	public void testEmbbedEnum_shouldSucceed() {

		assertTrue(DeliveryEntity.Fields.values().length == 3);
		assertThat(DeliveryEntity.Fields.valueOf("deliveryId"), is(DeliveryEntity.Fields.deliveryId));
		assertThat(DeliveryEntity.Fields.valueOf("packages"), is(DeliveryEntity.Fields.packages));
		assertThat(DeliveryEntity.Fields.valueOf("vehicleId"), is(DeliveryEntity.Fields.vehicleId));

	}

	@Test
	public void testGettersAndSetters() {

		DeliveryModel deliveryModel = createDeliveryModel();
		DeliveryEntity deliveryEntity = new DeliveryEntity(deliveryModel);

		List<PackageEntity> packages = Arrays.asList(createPackageEntity());
		deliveryEntity.setPackages(packages);
		assertThat(deliveryEntity, is(notNullValue()));

		assertThat(deliveryEntity.getDeliveryId(), is(deliveryModel.getDeliveryId()));
		assertThat(deliveryEntity.getVehicleId(), is(deliveryModel.getVehicle()));
		assertThat(deliveryEntity.getPackages(), is(notNullValue()));
		assertThat(deliveryEntity.getPackages().get(0), is(notNullValue()));
		assertThat(deliveryEntity.getPackages().get(0).getPackageId(), is(packages.get(0).getPackageId()));
		assertThat(deliveryEntity.getPackages().get(0).getWeight(), is(packages.get(0).getWeight()));

	}

	private PackageEntity createPackageEntity() {
		PackageEntity packageEntity = new PackageEntity();
		packageEntity.setPackageId("validId");
		packageEntity.setWeight(new BigDecimal(1.2));
		return packageEntity;
	}

	@Test
	public void publicConstructor_withParameters_shouldSucceed() {

		DeliveryModel deliveryModel = createDeliveryModel();
		DeliveryEntity deliveryEntity = new DeliveryEntity(deliveryModel);
		assertThat(deliveryEntity, is(notNullValue()));

		assertThat(deliveryEntity.getDeliveryId(), is(deliveryModel.getDeliveryId()));
		assertThat(deliveryEntity.getVehicleId(), is(deliveryModel.getVehicle()));

	}

	private DeliveryModel createDeliveryModel() {
		DeliveryModel deliveryModel = new DeliveryModel();
		deliveryModel.setDeliveryId("validDeliveryId");
		deliveryModel.setVehicle("validVehicleId");

		return deliveryModel;
	}
}
