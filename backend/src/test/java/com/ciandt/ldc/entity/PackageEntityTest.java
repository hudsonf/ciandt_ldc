package com.ciandt.ldc.entity;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.model.PackageModel;

@RunWith(MockitoJUnitRunner.class)
public class PackageEntityTest {

	@Test
	public void publicConstructor_withoutParameters_shouldSucceed() {

		assertThat(new PackageEntity(), is(notNullValue()));

	}

	@Test
	public void testEmbbedEnum_shouldSucceed() {

		assertTrue(PackageEntity.Fields.values().length == 3);
		assertThat(PackageEntity.Fields.valueOf("id"), is(PackageEntity.Fields.id));
		assertThat(PackageEntity.Fields.valueOf("packageId"), is(PackageEntity.Fields.packageId));
		assertThat(PackageEntity.Fields.valueOf("weight"), is(PackageEntity.Fields.weight));

	}

	@Test
	public void testGetSetId_shouldSucceed() {

		PackageEntity packageEntity = new PackageEntity();
		packageEntity.setId(1);
		assertThat(packageEntity.getId(), is(1L));
	}

	@Test
	public void publicConstructor_withParameters_shouldSucceed() {

		PackageModel packageModel = createPackageModel();
		DeliveryEntity deliveryEntity = createDeliveryEntity();
		PackageEntity packageEntity = new PackageEntity(packageModel, deliveryEntity);
		assertThat(packageEntity, is(notNullValue()));

		assertThat(packageEntity.getPackageId(), is(packageModel.getId()));
		assertThat(packageEntity.getWeight(), is(packageModel.getWeight()));

		assertThat(packageEntity.getDelivery().getDeliveryId(), is(deliveryEntity.getDeliveryId()));
		assertThat(packageEntity.getDelivery().getVehicleId(), is(deliveryEntity.getVehicleId()));

	}

	private PackageModel createPackageModel() {
		PackageModel packageModel = new PackageModel();
		packageModel.setId("validId");
		packageModel.setWeight(new BigDecimal(1.23));

		return packageModel;
	}

	private DeliveryEntity createDeliveryEntity() {
		DeliveryEntity deliveryEntity = new DeliveryEntity();
		deliveryEntity.setDeliveryId("validDeliveryId");
		deliveryEntity.setVehicleId("validVehicleId");

		return deliveryEntity;
	}
}
