package com.ciandt.ldc.helper;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.dao.DeliveryDAO;
import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.enums.DeliveryStateRestriction;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.PackageModel;

@RunWith(MockitoJUnitRunner.class)
public class ValidationHelperTest {

	@Mock
	private DeliveryDAO deliveryDAO;

	private static String DELIVERY_ID = "deliveryId";
	private static String VEHICLE_ID = "vehicleId";

	private static String PACKAGE_ID = "packageId";

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor<ValidationHelper> constructor = ValidationHelper.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		constructor.newInstance();
	}

	@Test
	public void validate_withDeliveryModelNull_shouldThrowException() {

		try {
			ValidationHelper.validate(null, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withDeliveryModelEmpty_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			ValidationHelper.validate(new DeliveryModel(), deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withInvalidDeliveryAndVehicleIds_shouldThrowException() {

		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId("Invalid / @#$");
			deliveryModel.setVehicle("Invalid / @#$");
			ValidationHelper.validate(deliveryModel, null);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withValueForDeliveryModelButNotForPackages_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withValueForDeliveryModelAndEmptyPackageList_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			deliveryModel.setPackages(new ArrayList<>());
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withEmptyPackage_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			deliveryModel.setPackages(Arrays.asList(new PackageModel()));
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withInvalidPackageIds_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			deliveryModel.setPackages(
					Arrays.asList(new PackageModel("/!@#", BigDecimal.ONE), new PackageModel("#$% /", BigDecimal.ONE)));
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withDuplicatedPackageIds_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			deliveryModel.setPackages(Arrays.asList(new PackageModel(PACKAGE_ID, BigDecimal.ONE),
					new PackageModel(PACKAGE_ID, BigDecimal.ONE)));
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withInvalidWeight_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			deliveryModel.setVehicle(VEHICLE_ID);
			deliveryModel.setPackages(Arrays.asList(new PackageModel(PACKAGE_ID, new BigDecimal(-1)),
					new PackageModel(PACKAGE_ID, BigDecimal.ZERO)));
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withValidData_shouldExecute() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);

		DeliveryModel deliveryModel = new DeliveryModel();
		deliveryModel.setDeliveryId(DELIVERY_ID);
		deliveryModel.setVehicle(VEHICLE_ID);
		deliveryModel.setPackages(Arrays.asList(new PackageModel("PackageIdOne", BigDecimal.ONE),
				new PackageModel("PackageIdTwo", BigDecimal.ONE)));
		ValidationHelper.validate(deliveryModel, deliveryDAO);

	}

	@Test
	public void validate_withExistingEntity_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(new DeliveryEntity());
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void validate_withDeliveryModelValidDeliveryIdOnly_shouldThrowException() {

		when(deliveryDAO.get(any(String.class))).thenReturn(null);
		try {
			DeliveryModel deliveryModel = new DeliveryModel();
			deliveryModel.setDeliveryId(DELIVERY_ID);
			ValidationHelper.validate(deliveryModel, deliveryDAO);
		} catch (DeliveryStateException exception) {
			assertThat(exception.getRestriction(), is(DeliveryStateRestriction.INCONSISTENT_DATA));
		}
	}

	@Test
	public void hasListDuplicatedIds_withDuplicatedIds_shouldReturnTrue() {

		boolean result = ValidationHelper.hasListDuplicatedIds(Arrays
				.asList(new PackageModel(PACKAGE_ID, BigDecimal.ONE), new PackageModel(PACKAGE_ID, BigDecimal.ONE)));
		assertThat(result, is(true));
	}

	@Test
	public void hasListDuplicatedIds_withDifferentIds_shouldReturnFalse() {

		boolean result = ValidationHelper.hasListDuplicatedIds(
				Arrays.asList(new PackageModel("Id", BigDecimal.ONE), new PackageModel("OtherId", BigDecimal.ONE)));
		assertThat(result, is(false));
	}

}
