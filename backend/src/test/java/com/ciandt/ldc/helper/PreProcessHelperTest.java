package com.ciandt.ldc.helper;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.PackageModel;

@RunWith(MockitoJUnitRunner.class)
public class PreProcessHelperTest {

	private static String DELIVERY_ID_LEADING_AND_TRAILLING_SPACE = " deliveryId ";
	private static String VEHICLE_ID_LEADING_AND_TRAILLING_SPACE = " vehicleId ";

	private static String PACKAGE_ID_LEADING_AND_TRAILLING_SPACE = " packageId ";

	@Test
	public void preProcess_withParameters_shouldSucceed() {

		DeliveryModel deliveryModel = createDeliveryModel();
		PreProcessHelper.preProcess(deliveryModel);
		assertThat(deliveryModel, is(notNullValue()));

		assertThat(deliveryModel.getDeliveryId(), equalTo(DELIVERY_ID_LEADING_AND_TRAILLING_SPACE.trim()));
		assertThat(deliveryModel.getVehicle(), equalTo(VEHICLE_ID_LEADING_AND_TRAILLING_SPACE.trim()));
		assertThat(deliveryModel.getPackages().get(0).getId(), equalTo(PACKAGE_ID_LEADING_AND_TRAILLING_SPACE.trim()));

	}

	private DeliveryModel createDeliveryModel() {
		DeliveryModel deliveryModel = new DeliveryModel();
		deliveryModel.setDeliveryId(DELIVERY_ID_LEADING_AND_TRAILLING_SPACE);
		deliveryModel.setVehicle(VEHICLE_ID_LEADING_AND_TRAILLING_SPACE);

		deliveryModel.setPackages(Arrays.asList(new PackageModel(PACKAGE_ID_LEADING_AND_TRAILLING_SPACE, null)));
		return deliveryModel;
	}

	@Test
	public void deliveryModel_withNull_keepNull() {

		DeliveryModel deliveryModel = null;
		PreProcessHelper.preProcess(deliveryModel);
		assertThat(deliveryModel, is(nullValue()));

	}

	@Test
	public void deliveryModel_withoutIds_keepSame() {

		DeliveryModel deliveryModel = new DeliveryModel();
		PreProcessHelper.preProcess(deliveryModel);
		assertThat(deliveryModel.getDeliveryId(), is(nullValue()));
		assertThat(deliveryModel.getVehicle(), is(nullValue()));

	}

	@Test
	public void packageModel_withoutIds_keepSame() {

		PackageModel packageModel = new PackageModel();
		PreProcessHelper.preProcess(packageModel);
		assertThat(packageModel.getId(), is(nullValue()));

	}

	@Test
	public void packageModel_withoutNull_keepSame() {

		PackageModel packageModel = null;
		PreProcessHelper.preProcess(packageModel);
		assertThat(packageModel, is(nullValue()));

	}

	@Test
	public void testConstructorIsPrivate()
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		Constructor<PreProcessHelper> constructor = PreProcessHelper.class.getDeclaredConstructor();
		assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		constructor.setAccessible(true);
		constructor.newInstance();
	}
}
