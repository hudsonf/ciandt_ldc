package com.ciandt.ldc.dao;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.model.PackageModel;

@RunWith(MockitoJUnitRunner.class)
public class PackageDAOImplTest {

	@InjectMocks
	private PackageDAOImpl packageDAO;

	@Mock
	protected SessionFactory sessionFactory;

	@Mock
	private Criteria criteria;

	@Mock
	private Session session;

	@Test
	public void create_validPackageModel_shouldReturnEntity() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);

		doNothing().when(session).persist(any(DeliveryEntity.class));

		assertThat(packageDAO.create(new PackageModel(), new DeliveryEntity()), notNullValue());

	}

	@Test
	public void create_validPackageModelList_shouldReturnEntityList() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);

		doNothing().when(session).persist(any(DeliveryEntity.class));

		assertThat(packageDAO.create(Arrays.asList(new PackageModel()), new DeliveryEntity()), notNullValue());

	}

	@Test
	public void create_EmptyPackageModelList_shouldEmptyList() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);

		doNothing().when(session).persist(any(DeliveryEntity.class));

		assertTrue(packageDAO.create(new ArrayList<>(), new DeliveryEntity()).size() == 0);

	}
}
