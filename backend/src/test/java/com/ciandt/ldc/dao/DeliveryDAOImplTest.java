package com.ciandt.ldc.dao;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.model.DeliveryModel;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryDAOImplTest {

	@InjectMocks
	private DeliveryDAOImpl deliveryDAO;

	@Mock
	protected SessionFactory sessionFactory;

	@Mock
	private Criteria criteria;

	@Mock
	private Session session;

	@Test
	public void get_withInvalidId_shouldReturnNull() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);
		when(session.createCriteria(DeliveryEntity.class)).thenReturn(criteria);

		when(criteria.uniqueResult()).thenReturn(null);

		assertThat(deliveryDAO.get("invalidId"), nullValue());

	}

	@Test
	public void get_withValidId_shouldReturnEntity() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);
		when(session.createCriteria(DeliveryEntity.class)).thenReturn(criteria);

		when(criteria.uniqueResult()).thenReturn(new DeliveryEntity());

		assertThat(deliveryDAO.get("valid"), notNullValue());

	}

	@Test
	public void create_validDeliveryModel_shouldReturnEntity() {

		when(sessionFactory.getCurrentSession()).thenReturn(session);

		doNothing().when(session).persist(any(DeliveryEntity.class));

		assertThat(deliveryDAO.create(new DeliveryModel()), notNullValue());

	}

}
