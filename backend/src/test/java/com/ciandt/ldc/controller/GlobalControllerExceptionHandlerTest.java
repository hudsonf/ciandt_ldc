package com.ciandt.ldc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ciandt.ldc.enums.DeliveryStateRestriction;
import com.ciandt.ldc.exception.DeliveryStateException;

@RunWith(MockitoJUnitRunner.class)
public class GlobalControllerExceptionHandlerTest {

	@InjectMocks
	private GlobalControllerExceptionHandler controller;

	@Test
	public void handleGenericException_shouldReturnInternalServerError() {

		ResponseEntity response = controller.handleGenericStateException(new Exception("Message"));

		assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));

	}

	@Test(expected = NullPointerException.class)
	public void handleDeliveryStateException_withNullRestriction_shouldReturnException() {

		ResponseEntity response = controller
				.handleUserStateException(new DeliveryStateException(null, new ArrayList<>()));

	}

	@Test
	public void handleDeliveryStateException_withUnknownReasonRestriction_shouldInternalServerError() {

		ResponseEntity response = controller.handleUserStateException(
				new DeliveryStateException(DeliveryStateRestriction.UNKNOW_REASON, new ArrayList<>()));

		assertThat(response.getStatusCode(), is(HttpStatus.INTERNAL_SERVER_ERROR));

	}

	@Test
	public void handleDeliveryStateException_withInconsistentDataRestriction_shouldReturnBadRequest() {

		ResponseEntity response = controller.handleUserStateException(
				new DeliveryStateException(DeliveryStateRestriction.INCONSISTENT_DATA, new ArrayList<>()));

		assertThat(response.getStatusCode(), is(HttpStatus.BAD_REQUEST));

	}

	@Test
	public void handleDeliveryStateException_withNotFoundRestriction_shouldReturnNotFound() {

		ResponseEntity response = controller.handleUserStateException(
				new DeliveryStateException(DeliveryStateRestriction.NOT_FOUND, new ArrayList<>()));

		assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));

	}

}
