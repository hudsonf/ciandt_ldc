package com.ciandt.ldc.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ciandt.ldc.bll.DeliveryBLL;
import com.ciandt.ldc.enums.DeliveryStateRestriction;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.StepModel;

@RunWith(MockitoJUnitRunner.class)
public class DeliveryControllerTest {

	@InjectMocks
	private DeliveryController controller;

	@Mock
	private DeliveryBLL deliveryBLL;

	@Test(expected = DeliveryStateException.class)
	public void receiveDelivery_withoutData_shouldThrowError() {

		when(deliveryBLL.receiveDelivery(any(DeliveryModel.class)))
				.thenThrow(new DeliveryStateException(DeliveryStateRestriction.INCONSISTENT_DATA, Arrays.asList("")));

		controller.receiveDelivery(null);

	}

	@Test
	public void receiveDelivery_withDummyData_shouldSucceed() {

		when(deliveryBLL.receiveDelivery(any(DeliveryModel.class))).thenReturn(true);

		ResponseEntity<?> response = controller.receiveDelivery(new DeliveryModel());

		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));

	}

	@Test(expected = DeliveryStateException.class)
	public void getDeliverySteps_withInvalidId_shouldThrowError() {

		when(deliveryBLL.createSteps(any(String.class)))
				.thenThrow(new DeliveryStateException(DeliveryStateRestriction.NOT_FOUND, Arrays.asList("")));

		controller.getDeliverySteps(null);

	}

	@Test
	public void getDeliverySteps_withValidId_shouldSucceed() {

		when(deliveryBLL.createSteps(any(String.class))).thenReturn(new ArrayList<StepModel>());

		ResponseEntity<?> response = controller.getDeliverySteps("ASD");

		assertThat(response.getStatusCode(), is(HttpStatus.OK));

	}

}
