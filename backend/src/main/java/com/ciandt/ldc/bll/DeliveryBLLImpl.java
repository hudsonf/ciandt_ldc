package com.ciandt.ldc.bll;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ciandt.ldc.dao.DeliveryDAO;
import com.ciandt.ldc.dao.PackageDAO;
import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.enums.DeliveryStateRestriction;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.helper.PreProcessHelper;
import com.ciandt.ldc.helper.ValidationHelper;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.ModelFactory;
import com.ciandt.ldc.model.ReturnModel;
import com.ciandt.ldc.model.StepModel;

@Transactional
@Service
public class DeliveryBLLImpl implements DeliveryBLL {

	@Autowired
	private DeliveryDAO deliveryDAO;

	@Autowired
	private PackageDAO packageDAO;

	@Override
	public boolean receiveDelivery(DeliveryModel deliveryModel) {
		PreProcessHelper.preProcess(deliveryModel);
		ValidationHelper.validate(deliveryModel, deliveryDAO);

		// store delivery
		DeliveryEntity deliveryEntity = deliveryDAO.create(deliveryModel);

		// store packages
		packageDAO.create(deliveryModel.getPackages(), deliveryEntity);

		return true;
	}

	@Override
	public List<StepModel> createSteps(String deliveryId) {

		DeliveryEntity deliveryEntity = deliveryDAO.get(deliveryId);

		if (deliveryEntity == null) {
			throw new DeliveryStateException(DeliveryStateRestriction.NOT_FOUND,
					Arrays.asList("Entrega não cadastrada"));
		}

		DeliveryModel deliveryModel = ModelFactory.convert(deliveryEntity,
				ReturnModel.getDeliveryModelWithPackageList());

		return TransferPackageProcedure.createSteps(deliveryModel.getPackages());
	}

}
