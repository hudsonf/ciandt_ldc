package com.ciandt.ldc.bll;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import org.springframework.util.CollectionUtils;

import com.ciandt.ldc.enums.LogisticCenterZoneEnum;
import com.ciandt.ldc.model.PackageModel;
import com.ciandt.ldc.model.StepModel;

public class TransferPackageProcedure {

	private Stack<PackageModel> supplyZone = new Stack<>();
	private Stack<PackageModel> transferZone = new Stack<>();
	private Stack<PackageModel> truckZone = new Stack<>();

	private List<StepModel> steps = new ArrayList<>();

	private long stepNumber = 1;

	/**
	 * Static method to generate the steps based on a list of {@link PackageModel}
	 *
	 * @param packages
	 *            list of {@link PackageModel} that will be created the procedure
	 * @return List of created {@link StepModel}
	 * 
	 */
	public static List<StepModel> createSteps(List<PackageModel> packages) {

		if (CollectionUtils.isEmpty(packages)) {
			return new ArrayList<>();
		}

		TransferPackageProcedure transferPackageProcedure = new TransferPackageProcedure(packages);

		return transferPackageProcedure.getSteps();

	}

	/**
	 * Private constructor to sort and create the call the recursive algorithm
	 *
	 * @param packages
	 *            list of {@link PackageModel} that will be created the procedure
	 *
	 */
	private TransferPackageProcedure(List<PackageModel> packages) {
		// As the documentaion said, it is guaranteed that supply area will pilled the
		// boxes according to their weight.
		// So if the post don't have the weights sorted, they will always be sorted at
		// this moment.

		Collections.sort(packages, new Comparator<PackageModel>() {
			@Override
			public int compare(PackageModel first, PackageModel second) {
				return second.getWeight().compareTo(first.getWeight());
			}
		});

		packages.stream().forEach(packageUnit -> supplyZone.push(packageUnit));

		towerOfHanoi(packages.size(), LogisticCenterZoneEnum.SUPPLY_ZONE, LogisticCenterZoneEnum.TRUCK_ZONE,
				LogisticCenterZoneEnum.TRANSFER_ZONE);

	}

	/**
	 * Recursive algorithm to resolve Hanoi Tower operation using three stacks
	 *
	 * @param packagesToMove
	 *            {@link int} value of number of packages to move
	 * @param from
	 *            what stack will be moved type of {@link LogisticCenterZoneEnum}
	 * @param to
	 *            what stack will be moved type of {@link LogisticCenterZoneEnum}
	 * @param aux
	 *            auxiliary stack to do the operation {@link LogisticCenterZoneEnum}
	 *
	 */
	private void towerOfHanoi(int packagesToMove, LogisticCenterZoneEnum from, LogisticCenterZoneEnum to,
			LogisticCenterZoneEnum aux) {
		if (packagesToMove == 1) {
			recordStep(from, to);
			return;
		}
		towerOfHanoi(packagesToMove - 1, from, aux, to);
		recordStep(from, to);
		towerOfHanoi(packagesToMove - 1, aux, to, from);
	}

	/**
	 * Do the package movement and stores a {@link StepModel} in the step list
	 *
	 * @param from
	 *            what stack will be moved type of {@link LogisticCenterZoneEnum}
	 * @param to
	 *            what stack will be moved type of {@link LogisticCenterZoneEnum}
	 *
	 */
	private void recordStep(LogisticCenterZoneEnum from, LogisticCenterZoneEnum to) {
		PackageModel packageMoved = null;
		switch (from) {
		case SUPPLY_ZONE:
			packageMoved = supplyZone.pop();
			break;
		case TRANSFER_ZONE:
			packageMoved = transferZone.pop();
			break;
		case TRUCK_ZONE:
			packageMoved = truckZone.pop();
			break;

		default:
			break;
		}

		switch (to) {
		case SUPPLY_ZONE:
			supplyZone.push(packageMoved);
			break;
		case TRANSFER_ZONE:
			transferZone.push(packageMoved);
			break;
		case TRUCK_ZONE:
			truckZone.push(packageMoved);
			break;

		default:
			break;
		}

		steps.add(new StepModel(stepNumber++, packageMoved.getId(), from, to));

	}

	private List<StepModel> getSteps() {

		return steps;
	}

}
