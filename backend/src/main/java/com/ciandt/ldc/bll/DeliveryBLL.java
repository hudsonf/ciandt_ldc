package com.ciandt.ldc.bll;

import java.util.List;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.StepModel;

public interface DeliveryBLL {

	/**
	 * Receives a {@link DeliveryModel}, process, validates and stores as
	 * {@link DeliveryEntity}
	 *
	 * @param deliveryModel
	 * @exception throws
	 *                {@link DeliveryStateException} if validation fails
	 * @return true if the procedure executed, otherwise throws a
	 *         {@link DeliveryStateException}
	 */
	boolean receiveDelivery(DeliveryModel deliveryModel);

	/**
	 * Get the {@link DeliveryEntity} based on the Id and create the steps as a list
	 * of {@link StepModel}
	 *
	 * @param deliveryModel
	 *            Receives a {@link String} and generates a list of
	 *            {@link StepModel}
	 * @return List of created {@link StepModel}
	 */
	List<StepModel> createSteps(String deliveryId);

}
