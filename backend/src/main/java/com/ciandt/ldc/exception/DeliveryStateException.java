package com.ciandt.ldc.exception;

import java.util.ArrayList;
import java.util.List;

import com.ciandt.ldc.enums.DeliveryStateRestriction;

public class DeliveryStateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final DeliveryStateRestriction restriction;

	private List<String> errors = new ArrayList<String>();

	public List<String> getErrors() {
		return errors;
	}

	public DeliveryStateRestriction getRestriction() {
		return restriction;
	}

	public DeliveryStateException(DeliveryStateRestriction restriction, List<String> errors) {
		this.restriction = restriction;
		this.errors = errors;

	}
}
