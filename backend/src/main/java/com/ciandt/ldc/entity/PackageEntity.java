package com.ciandt.ldc.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ciandt.ldc.model.PackageModel;

@Entity
@Table(name = "package")
public class PackageEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(columnDefinition = "text", nullable = false, name = "package_id")
	private String packageId;

	@Column(name = "weight", nullable = false)
	private BigDecimal weight;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivery_id")
	private DeliveryEntity delivery;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public DeliveryEntity getDelivery() {
		return delivery;
	}

	public void setDelivery(DeliveryEntity delivery) {
		this.delivery = delivery;
	}

	public enum Fields {
		id, packageId, weight
	}

	public PackageEntity() {
	}

	public PackageEntity(PackageModel packageModel, DeliveryEntity deliveryEntity) {
		setPackageId(packageModel.getId());
		setWeight(packageModel.getWeight());
		setDelivery(deliveryEntity);
	}

}
