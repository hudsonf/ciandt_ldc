package com.ciandt.ldc.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ciandt.ldc.model.DeliveryModel;

@Entity
@Table(name = "delivery")
public class DeliveryEntity {

	@Id
	@Column(columnDefinition = "text", nullable = false, name = "delivery_id")
	private String deliveryId;

	@Column(columnDefinition = "text", nullable = false, name = "vehicle_id")
	private String vehicleId;

	@OneToMany(mappedBy = "delivery", fetch = FetchType.LAZY)
	private List<PackageEntity> packages;

	public String getDeliveryId() {
		return deliveryId;
	}

	public void setDeliveryId(String deliveryId) {
		this.deliveryId = deliveryId;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public List<PackageEntity> getPackages() {
		return packages;
	}

	public void setPackages(List<PackageEntity> packages) {
		this.packages = packages;
	}

	public enum Fields {
		deliveryId, vehicleId, packages
	}

	public DeliveryEntity() {

	}

	public DeliveryEntity(DeliveryModel deliveryModel) {
		setDeliveryId(deliveryModel.getDeliveryId());
		setVehicleId(deliveryModel.getVehicle());
	}
}
