package com.ciandt.ldc.dao;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.model.DeliveryModel;

public interface DeliveryDAO {

	/**
	 * Get the {@link DeliveryEntity} based on the Id
	 *
	 * @param deliveryId
	 * @return {@link DeliveryEntity}
	 */
	DeliveryEntity get(String deliveryId);

	/**
	 * Create a {@link DeliveryEntity} based on a {@link DeliveryModel}
	 *
	 * @param deliveryModel
	 *            {@link DeliveryModel}
	 * @return {@link DeliveryEntity}
	 */
	DeliveryEntity create(DeliveryModel deliveryModel);

}
