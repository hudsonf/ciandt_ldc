package com.ciandt.ldc.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.entity.PackageEntity;
import com.ciandt.ldc.model.PackageModel;

@Repository
public class PackageDAOImpl extends BaseDAO implements PackageDAO {

	@Override
	public ArrayList<PackageEntity> create(List<PackageModel> packages, DeliveryEntity deliveryEntity) {
		if (CollectionUtils.isEmpty(packages)) {
			return new ArrayList<>();
		}

		return (ArrayList<PackageEntity>) packages.stream().map(packageModel -> create(packageModel, deliveryEntity))
				.collect(Collectors.toList());
	}

	public PackageEntity create(PackageModel packageModel, DeliveryEntity deliveryEntity) {
		PackageEntity packageEntity = new PackageEntity(packageModel, deliveryEntity);

		getSession().persist(packageEntity);

		return packageEntity;
	}

}
