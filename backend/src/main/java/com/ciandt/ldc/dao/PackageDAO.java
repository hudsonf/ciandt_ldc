package com.ciandt.ldc.dao;

import java.util.List;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.entity.PackageEntity;
import com.ciandt.ldc.model.PackageModel;

public interface PackageDAO {

	/**
	 * Create a list of {@link PackageEntity} based on a list of
	 * {@link PackageModel}
	 *
	 * @param packages
	 *            {@link PackageModel}
	 * @param deliveryEntity
	 *            {@link DeliveryEntity}
	 * @return list of {@link PackageEntity}
	 */
	List<PackageEntity> create(List<PackageModel> packages, DeliveryEntity deliveryEntity);

	/**
	 * Create the {@link PackageEntity} based on a {@link PackageModel}
	 *
	 * @param packageModel
	 *            {@link PackageModel}
	 * @param deliveryEntity
	 *            {@link DeliveryEntity}
	 * @return {@link PackageEntity}
	 */
	PackageEntity create(PackageModel packageModel, DeliveryEntity deliveryEntity);

}
