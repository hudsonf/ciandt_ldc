package com.ciandt.ldc.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.model.DeliveryModel;

@Repository
public class DeliveryDAOImpl extends BaseDAO implements DeliveryDAO {

	@Override
	public DeliveryEntity get(String deliveryId) {
		Criteria criteria = getSession().createCriteria(DeliveryEntity.class);

		criteria.add(Restrictions.eq(DeliveryEntity.Fields.deliveryId.name(), deliveryId));

		return (DeliveryEntity) criteria.uniqueResult();

	}

	@Override
	public DeliveryEntity create(DeliveryModel deliveryModel) {
		DeliveryEntity deliveryEntity = new DeliveryEntity(deliveryModel);

		getSession().persist(deliveryEntity);

		return deliveryEntity;
	}

}
