package com.ciandt.ldc.enums;

public enum LogisticCenterZoneEnum {

	TRUCK_ZONE("zona do caminhão"), TRANSFER_ZONE("zona de transferência"), SUPPLY_ZONE("zona de abastecimento");

	LogisticCenterZoneEnum(String name) {
		this.name = name;
	}

	private String name;

	public String getName() {
		return name;
	}

}
