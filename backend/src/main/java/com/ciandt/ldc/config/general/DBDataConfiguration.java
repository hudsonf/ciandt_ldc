package com.ciandt.ldc.config.general;

public class DBDataConfiguration {

	private DBDataConfiguration() {

	}

	public static String getDBUrl() {
		return System.getenv("DB_URL");
	}

	public static String getDBUsername() {

		return System.getenv("DB_USERNAME");
	}

	public static String getDBPassword() {
		return System.getenv("DB_PASSWORD");
	}
}
