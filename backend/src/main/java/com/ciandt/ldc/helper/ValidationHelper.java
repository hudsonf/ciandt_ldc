package com.ciandt.ldc.helper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.ciandt.ldc.dao.DeliveryDAO;
import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.enums.DeliveryStateRestriction;
import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.PackageModel;

public class ValidationHelper {

	private ValidationHelper() {

	}

	public static void validate(DeliveryModel deliveryModel, DeliveryDAO deliveryDAO) {
		List<String> errors = new ArrayList<>();
		if (deliveryModel == null) {
			errors.add("Objeto recebido nulo.");
			throw new DeliveryStateException(DeliveryStateRestriction.INCONSISTENT_DATA, errors);
		}

		if (StringUtils.isEmpty(deliveryModel.getDeliveryId()) || !isValidId(deliveryModel.getDeliveryId())) {
			errors.add("DeliveryId Inválido.");

		} else {

			DeliveryEntity deliveryEntity = deliveryDAO.get(deliveryModel.getDeliveryId());
			if (deliveryEntity != null) {
				errors.add("DeliveryId já existente.");
			}
		}

		if (StringUtils.isEmpty(deliveryModel.getVehicle()) || !isValidId(deliveryModel.getVehicle())) {
			errors.add("Vehicle Id Inválido.");
		}

		if (CollectionUtils.isEmpty(deliveryModel.getPackages())) {
			errors.add("Lista de pacotesInvalida.");
		} else {
			validate(deliveryModel.getPackages(), errors);
		}

		if (!CollectionUtils.isEmpty(errors)) {
			throw new DeliveryStateException(DeliveryStateRestriction.INCONSISTENT_DATA, errors);
		}

	}

	private static boolean isValidId(String id) {
		Pattern special = Pattern.compile("[!@#$%&*()/_+=| <>?{}\\[\\]~]");
		Matcher hasSpecial = special.matcher(id);
		return !hasSpecial.find();
	}

	private static void validate(List<PackageModel> packages, List<String> errors) {
		if (hasListDuplicatedIds(packages)) {
			errors.add("Package Id duplicados.");
		}

		int packagesWithoutId = 0, packagesWithouWeight = 0, packagesWithInvalidWeight = 0;

		for (PackageModel packageUnit : packages) {
			if (StringUtils.isEmpty(packageUnit.getId()) || !isValidId(packageUnit.getId())) {
				packagesWithoutId++;
			}
			if (packageUnit.getWeight() == null) {
				packagesWithouWeight++;
			} else if (packageUnit.getWeight().compareTo(BigDecimal.ZERO) <= 0) {
				packagesWithInvalidWeight++;
			}
		}

		if (packagesWithoutId > 0) {
			errors.add(packagesWithoutId + " package(s) com Id nulo(s) ou inválidos.");
		}

		if (packagesWithouWeight > 0) {
			errors.add(packagesWithouWeight + " package(s) sem peso definido.");
		}

		if (packagesWithInvalidWeight > 0) {
			errors.add(packagesWithouWeight + " package(s) com peso inválido.");
		}

	}

	public static boolean hasListDuplicatedIds(List<PackageModel> packages) {
		return packages.stream().map(packageUnit -> packageUnit.getId()).collect(Collectors.toSet()).size() < packages
				.size();
	}

}
