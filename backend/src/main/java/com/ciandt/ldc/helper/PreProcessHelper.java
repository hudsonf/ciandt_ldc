package com.ciandt.ldc.helper;

import org.springframework.util.CollectionUtils;

import com.ciandt.ldc.model.DeliveryModel;
import com.ciandt.ldc.model.PackageModel;

public class PreProcessHelper {

	private PreProcessHelper() {

	}

	public static void preProcess(DeliveryModel deliveryModel) {
		if (deliveryModel == null) {
			return;
		}

		if (deliveryModel.getDeliveryId() != null) {
			deliveryModel.setDeliveryId(deliveryModel.getDeliveryId().trim());
		}

		if (deliveryModel.getVehicle() != null) {
			deliveryModel.setVehicle(deliveryModel.getVehicle().trim());
		}

		if (!CollectionUtils.isEmpty(deliveryModel.getPackages())) {
			deliveryModel.getPackages().stream().forEach(packageUnit -> preProcess(packageUnit));
		}

	}

	public static void preProcess(PackageModel packageUnit) {
		if (packageUnit == null) {
			return;
		}

		if (packageUnit.getId() != null) {
			packageUnit.setId(packageUnit.getId().trim());
		}
	}

}
