package com.ciandt.ldc.controller;

import java.util.Arrays;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.ciandt.ldc.exception.DeliveryStateException;
import com.ciandt.ldc.model.ModelErrorReturnRest;

@ControllerAdvice
@EnableWebMvc
public class GlobalControllerExceptionHandler {

	@ExceptionHandler(value = DeliveryStateException.class)
	public ResponseEntity handleUserStateException(DeliveryStateException exception) {

		switch (exception.getRestriction()) {
		case INCONSISTENT_DATA:
			return new ResponseEntity<>(new ModelErrorReturnRest(exception.getErrors()), HttpStatus.BAD_REQUEST);
		case NOT_FOUND:
			return new ResponseEntity<>(new ModelErrorReturnRest(exception.getErrors()), HttpStatus.NOT_FOUND);

		default:
			return new ResponseEntity<>(new ModelErrorReturnRest(exception.getErrors()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity handleGenericStateException(Exception exception) {
		return new ResponseEntity<>(new ModelErrorReturnRest(Arrays.asList("Exceção inesperada.")),
				HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
