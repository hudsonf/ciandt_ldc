package com.ciandt.ldc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ciandt.ldc.bll.DeliveryBLL;
import com.ciandt.ldc.model.DeliveryModel;

@Controller
@RequestMapping("delivery")
public class DeliveryController {

	@Autowired
	private DeliveryBLL deliveryBLL;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity receiveDelivery(@RequestBody DeliveryModel deliveryModel) {

		deliveryBLL.receiveDelivery(deliveryModel);
		return new ResponseEntity<>(HttpStatus.CREATED);

	}

	@RequestMapping(value = "/{deliveryId}/step", method = RequestMethod.GET)
	public ResponseEntity getDeliverySteps(@PathVariable("deliveryId") String deliveryId) {

		return new ResponseEntity<>(deliveryBLL.createSteps(deliveryId), HttpStatus.OK);

	}

}
