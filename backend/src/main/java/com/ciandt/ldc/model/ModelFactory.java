package com.ciandt.ldc.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.CollectionUtils;

import com.ciandt.ldc.entity.DeliveryEntity;
import com.ciandt.ldc.entity.PackageEntity;

public class ModelFactory {

	private ModelFactory() {

	}

	public static DeliveryModel convert(DeliveryEntity deliveryEntity, DeliveryModel returnModel) {
		if (deliveryEntity == null || returnModel == null) {
			return null;
		}

		DeliveryModel deliveryModel = new DeliveryModel();
		deliveryModel.setDeliveryId(deliveryEntity.getDeliveryId());
		deliveryModel.setVehicle(deliveryEntity.getVehicleId());
		deliveryModel.setPackages(convert(deliveryEntity.getPackages(), returnModel.getPackages()));

		return deliveryModel;
	}

	private static List<PackageModel> convert(List<PackageEntity> packages, List<PackageModel> packagesModel) {
		if (CollectionUtils.isEmpty(packages) || CollectionUtils.isEmpty(packagesModel)) {
			return new ArrayList<>();
		}

		return packages.stream().map(packageEntity -> convert(packageEntity, packagesModel.get(0)))
				.collect(Collectors.toList());
	}

	public static PackageModel convert(PackageEntity packageEntity, PackageModel returnModel) {
		if (packageEntity == null || returnModel == null) {
			return null;
		}

		PackageModel packageModel = new PackageModel();
		packageModel.setId(packageEntity.getPackageId());
		packageModel.setWeight(packageEntity.getWeight());

		return packageModel;
	}

}
