package com.ciandt.ldc.model;

import java.math.BigDecimal;

public class PackageModel {

	private String id;

	private BigDecimal weight;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public PackageModel(String id, BigDecimal weight) {
		setId(id);
		setWeight(weight);
	}

	public PackageModel() {
	}

}
