package com.ciandt.ldc.model;

import java.io.Serializable;
import java.util.List;

public class ModelErrorReturnRest implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<String> errors;

	void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

	public ModelErrorReturnRest(List<String> errors) {
		setErrors(errors);

	}
}
