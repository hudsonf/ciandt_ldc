package com.ciandt.ldc.model;

import java.util.Arrays;

public class ReturnModel {

	private ReturnModel() {

	}

	public static DeliveryModel getDeliveryModelWithPackageList() {
		DeliveryModel returnModel = new DeliveryModel();

		returnModel.setPackages(Arrays.asList(new PackageModel()));

		return returnModel;
	}

}
