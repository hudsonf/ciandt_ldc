package com.ciandt.ldc.model;

import com.ciandt.ldc.enums.LogisticCenterZoneEnum;

public class StepModel {

	private long step;

	private String packageId;

	private String from;

	private String to;

	public long getStep() {
		return step;
	}

	public void setStep(long step) {
		this.step = step;
	}

	public String getPackageId() {
		return packageId;
	}

	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public StepModel(long step, String packageId, LogisticCenterZoneEnum from, LogisticCenterZoneEnum to) {

		setStep(step);
		setPackageId(packageId);
		setFrom(from == null ? null : from.getName());
		setTo(to == null ? null : to.getName());

	}

}
