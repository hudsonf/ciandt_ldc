# CI&T Challenge - Logistics Distribution Center

This is the implementation of the CI&T - Logistics Distribution Center challenge done by Hudson de Almeida Ferreira.

Important notes and restrictions coverage

  - The data can't be lost: Since we are using a database to store valid deliveries, the data will be not lost.
  - Complex Scenarios tests, considering non-functional requisites: More description below.
  - Plan the deploy (12factor): More information below
  - Follow the interface (path, names, json etc): Done

# 12 Factor
1. Codebase: In bitbucket
2. Dependencies: explicit declared on pom.xml file and DBConfig
3. Config: Environment variables define the database connection. DB_URL, DB_USERNAME and DB_PASSWORD
4. Backing services: Considering we are using the database as a service, we can easily change it to MySQL instead of PostgreSQL since we are using JPA implementation (Hibernate) decoupling the database from the implementation.
5. Build, release, run: We are using Maven, so we can easily run the unit tests and create a build automatically.
6. Process: Stateless API
7. Port Biding: Using ApacheTomcat default 8080 port.
8. Concurrency: Relying on the database index to not insert 2 deliveries with the same deliveryId.
9. Disposability: Startup and shutdown a few seconds.
10. Dev/prod parity: Similar environment.
11. Logs: Not considered for this simple use case.
12. Admin Process: Not considered for this simple use case.


# Build with:

  - Java 8
  - Spring Framework
  - Hibernate 5.2.10
  - Apache Tomcat 9
  - Google Gson 2.8.2
  - JUnit 4.12
  - Mockito 1.9.5
  - PostgreSQL 9.4


# How to do the build and run the application in a development environment

Prerequisites:

  - Java 8
  - Apache Tomcat 9
  - Eclipse Oxygen for JEE(or other IDE)
  - PostgreSQL 9.4 or above

# Step 1:  Importing Project
  1. Import backend project as Maven project in Eclipse
  2. Right-click on the project and select update project in maven options
  3. Import Apache  as a server
  4. Right-click in the apache in Server tab and select Add/Remove
  5. Add "backend" to the right box.

# Step 2: Configuring Database connection
  1. Create a PostgreSQL database
  2. Set up the variables DB_URL, DB_USERNAME, DB_PASSWORD in your system environment or "Run Configurations" or "Debug Configurations". 
Value examples: 
DB_URL = "jdbc:postgresql://localhost:5432/postgres", 
DB_USERNAME = "username123"
DB_PASSWORD = "password123"

Obs: You can use mine to be easier.
DB_URL = "jdbc:postgresql://ciandt.cfz40tttvzqn.us-east-1.rds.amazonaws.com:5432/ciandt"
DB_USERNAME = "ciandt", DB_PASSWORD = "ciandtciandt"
Since it is the cheapest database in AWS, the application startup will be long, something close to 217000 ms.

Since the database exists, the table will be created on the startup.

# Step 3: Running application
  1. Right-click on the Apache Tomcat in Servers tab and select "Run"

The API will be available in the following URL
```sh
POST http://localhost:8080/backend/delivery
GET http://localhost:8080/backend/delivery/{deliveryId}/steps
```

# Step 4: Creating a build
  1. Right-click on the project, select Run As and then maven build.


# Non-functional requisites:

Usability
Since we are using REST and it is a wide known pattern, it is easy to learn and easy to use.

Maintenance 
Stateless application with segregation of Bussiness Logic Layer and Data Access Object layer, this increases the ease of maintenance and evolution

Reliability
REST API in Apache Tomcat server is a well known Java web server.
The code has 99.8% of code coverage in with units tests.

Performance
Good performance, the steps are only calculated when requested.

Portability
Create using Java 8 (JVM), which is naturally portable.

Reusability
The software was modeled using the open/close principle.

Security
Not considered.


#Execution Plan

1. Reading all given documentation (20 minutes)
2. Analysis of the challenge (20 minutes)
3. Mapping the problem as a Hanoi Tower (0 minutes - on the reading)
4. Looking for Hanoi Tower implementations (30 minutes)
5. Choosing the best implementation (20 minutes)
6. Reading the "Important Notes and Restrictions" section again (10 minutes)
7. Mapping which components must be included on the API. (20 minutes)
8. Deciding the stack of the application (30 minutes)
9. Creating the BitBucket git repository (2 minutes)
10. Adapting an existing application structure already owned by me (90 minutes)
11. Modeling the database. (20 minutes)
12. Creating the Delivery and Package model objects (10 minutes)
13. Creating the controller with ControllerAdivise (20 minutes)
14. Creating the Exception and Restriction (10minutes)
15. Implementing Hanoi Tower algorithm (90 minutes)
16. Creating unit tests in order to reach max code coverage (300 minutes)
17. Create Javadoc in interfaces and main methods. (50 minutes)
18. Complete Code review (100 minutes)
19. Create README file and swagger (240 minutes)
20. Set repository as public (1 minute)
20. Send e-mail to Davi Melazo (5 minutes)

Total: 1088 minutes or 18 hours and 8 minutes.



# Macro overview of the application deployment
First, you should test your application with, unit, coverage, integration, and functional tests, then you have a valid build to proceed with the deploy.

To deploy, you can:
Create an instance of a database as service using a cloud provider.
Create an instance of Elastic BeanStalk (AWS) like service with the DB configuration and metrics configuration as a micro-service.
Build the backend project with the command in eclipse described above.
It will generate a ".war" file that you can upload to the cloud and the service will be responsible to manage and scale.
In case that your database is not enough, you can use multi-az deployment, redundancy, migrate to a NoSQL.
Every time that you need to create a new deploy, you can rely on cloud services to keep your application running and update node by node.


# Swagger

API documentation in ldc.swagger


# Pre-process and validation considerations

  -The application is trimming vehicle, delivery and package ids.
  -If any Id contains one of these characters "!@#$%&*()/_+=| <>?{}[]~" the Id will be considered as invalid. Some of them need encoding and they shouldn't be used in Ids.
  -Delivery without any package is considered invalid.
  -Package without weight or, less or equals to zero will also be considered invalid.
  -Duplicated package Ids in the same delivery will invalidate the delivery.
  -The application will not store the same deliveryId twice. However, you can repeat the package Id (for different deliveries) and the vehicle id.


